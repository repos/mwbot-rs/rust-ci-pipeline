# rust-ci-pipeline

GitLab CI pipeline for Rust and [mwbot-rs](https://www.mediawiki.org/wiki/mwbot-rs) projects.

## `tool.yml`

This workflow is intended for Toolforge tools, but it should work for any Rust
project.

To use it, create the following as `.gitlab-ci.yml`:
```yaml
include:
  - 'https://gitlab.wikimedia.org/repos/mwbot-rs/rust-ci-pipeline/-/raw/main/tool.yml'

variables:
  CARGO_AUDIT: ""
  BINARY_NAME: "myproject"
```

It checks the following:

* Runs `rustfmt`
* Runs `cargo clippy`
* Runs `cargo test`
* Runs `cargo audit` to check dependencies against [Rust security advisories](https://rustsec.org/)
  * The `CARGO_AUDIT` variable can be used to pass arguments to ignore advisories, e.g. `CARGO_AUDIT: "--ignore RUSTSEC-2023-0000"`
* Runs `cargo test` under nightly (allowed to fail)
* Provides code coverage to GitLab (allowed to fail)
* If `BINARY_NAME` is set:
  * Runs `cargo build --release` and uploads the result as an artifact
    * On a tag push, the binary will be uploaded as a "GitLab Release".

## `jobs.yml`

This is specific to the mwbot-rs project and not really documented yet. 

## License and reuse
These pipelines are released under the Apache 2.0 license. Please fork and
modify as desired, with attribution.
