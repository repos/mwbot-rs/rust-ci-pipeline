# SPDX-License-Identifier: Apache-2.0
include:
  - project: repos/releng/docpub
    file: includes/publish.yml

stages:
  - build
  - deploy
  - publish

variables:
  MSRV: "1.56"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${BINARY_NAME}/${CI_COMMIT_TAG}"
  RUST_DISABLE_WINDOWS: "false"
  RUST_DISABLE_WASM: "false"
  WASM_EXCLUDE: ""
  CARGO_AUDIT_VERSION: "0.21.1"

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

rust-clippy-x86_64:
  stage: build
  image: rust:latest
  variables: &variables
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  tags:
    - memory-optimized
  script:
    - rustup component add rustfmt
    - rustup component add clippy
    - cargo install cargo-hack
    - cargo fmt -- --check
    - cargo hack clippy --feature-powerset --no-dev-deps -- -D warnings
  after_script: &cleanup
    - rm -rf target .cargo


rust-latest-x86_64:
  stage: build
  image: rust:latest
  variables: *variables
  tags:
    - memory-optimized
  script:
    - cargo test --all-features --verbose -- --test-threads=1
  after_script: *cleanup

rust-msrv-x86_64:
  stage: build
  image: rust:$MSRV
  tags:
    - memory-optimized
  script:
    - cargo test --all-features --verbose -- --test-threads=1
  after_script: *cleanup

rust-nightly:
  stage: build
  image: rustlang/rust:nightly
  variables: *variables
  tags:
    - memory-optimized
  script: &do
    - cargo build --all-features --verbose
    - cargo test --all-features --verbose -- --test-threads=1
  allow_failure: true
  after_script: *cleanup

rust-miri-mwtitle:
  stage: build
  image: rustlang/rust:nightly
  variables: *variables
  rules:
    - exists:
        - mwtitle/src/lib.rs
  script:
    - export MIRI_NIGHTLY=nightly-$(curl -s https://rust-lang.github.io/rustup-components-history/x86_64-unknown-linux-gnu/miri)
    - 'echo "Installing latest nightly with Miri: $MIRI_NIGHTLY"'
    - rustup set profile minimal
    - rustup override set "$MIRI_NIGHTLY"
    - rustup component add miri
    - cargo miri test -p mwtitle --lib --all-features --verbose
  allow_failure: true
  after_script: *cleanup

rust-binary-x86_64:
  stage: deploy
  image: rust:latest
  variables: *variables
  tags:
    - memory-optimized
  needs:
    - job: rust-latest-x86_64
      artifacts: false
    - job: rust-clippy-x86_64
      artifacts: false
  script:
    - |
      set -x
      args="--all-features"
      # Only add --locked if a lockfile exists
      if [[ -f "Cargo.lock" ]]; then
        args="${args} --locked"
      fi
      cargo build --release --verbose $args
    - mv "target/release/$BINARY_NAME" "${BINARY_NAME}-x86_64"
  rules:
    - if: $BINARY_NAME
  artifacts:
    paths:
      - "${BINARY_NAME}-x86_64"
  after_script: *cleanup

rust-security:
  stage: build
  image: rust:latest
  variables: *variables
  before_script:
    - wget https://github.com/rustsec/rustsec/releases/download/cargo-audit%2Fv${CARGO_AUDIT_VERSION}/cargo-audit-x86_64-unknown-linux-gnu-v${CARGO_AUDIT_VERSION}.tgz
    - tar xvf cargo-audit-x86_64-unknown-linux-gnu-v${CARGO_AUDIT_VERSION}.tgz -C "$CARGO_HOME/bin/" --strip-components=1
    - rm cargo-audit-x86_64-unknown-linux-gnu-v${CARGO_AUDIT_VERSION}.tgz
  script:
    - cargo audit -D warnings $CARGO_AUDIT
  after_script: *cleanup

docker:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  rules:
    - exists:
        - Dockerfile
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:latest

.rust-coverage:
  stage: build
  extends:
    - .docpub:build-docs
  image: rustlang/rust:nightly-bullseye
  variables:
    RUSTFLAGS: "-C instrument-coverage"
    LLVM_PROFILE_FILE: "coverage-%p-%m.profraw"
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
    DOCS_DIR: "public"
  tags:
    - memory-optimized
  script:
    - rustup component add llvm-tools-preview
    - wget https://github.com/mozilla/grcov/releases/download/v0.8.13/grcov-x86_64-unknown-linux-gnu.tar.bz2
    - tar xvf grcov-x86_64-unknown-linux-gnu.tar.bz2
    - mv grcov /usr/local/cargo/bin/ && rm grcov-x86_64-unknown-linux-gnu.tar.bz2
    - apt-get update && apt-get install --no-install-recommends -y lcov python3-pip
    - python3 -m pip install lcov_cobertura
    - cargo test --all-features -- --test-threads=1
    - grcov . --binary-path ./target/debug/ -s . -t html --branch --ignore-not-existing --ignore "*cargo*" -o ./coverage/
    - grcov . --binary-path ./target/debug/ -s . -t lcov --branch --ignore-not-existing --ignore "*cargo*" -o coverage.lcov
    - lcov_cobertura coverage.lcov
    - lcov --summary coverage.lcov
    - mkdir -p public
    - mv coverage/ public/
    - mv coverage.xml public/
    # Cannot use after_script, so manually repeat *cleanup
    - rm -rf target .cargo
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: public/coverage.xml

rustdoc:
  stage: build
  extends:
    - .docpub:build-docs
  image: rustlang/rust:nightly
  variables:
    # The CI job times out otherwise
    CARGO_BUILD_JOBS: "1"
    RUSTDOCFLAGS: --cfg docsrs
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
    DOCS_DIR: "public"
  script:
    - mkdir -p public
    - cargo doc --all-features --no-deps
    - mv target/doc/* public/
    # Cannot use after_script, so manually repeat *cleanup
    - rm -rf target .cargo

.publish-docs:
  stage: publish
  extends:
    - .docpub:publish-docs
  needs:
    - job: rustdoc
      artifacts: true
  only:
    - main

.publish-coverage:
  stage: publish
  variables:
    COVERAGE: "true"
  extends:
    - .docpub:publish-docs
  needs:
    - job: rust-coverage
      artifacts: true
  only:
    - main


# Cross-compile for armv7-unknown-linux-gnueabihf
rust-latest-armv7:
  stage: build
  image: rust:latest
  rules: &rules_only_on_tag
    - if: $CI_COMMIT_TAG
  variables:
    CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER: arm-linux-gnueabihf-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  tags:
    - memory-optimized
  script:
    - apt-get update -qq && apt-get install gcc-arm-linux-gnueabihf -y -qq
    - rustup target add armv7-unknown-linux-gnueabihf
    - rustup component add clippy
    - cargo clippy --target armv7-unknown-linux-gnueabihf --all-features -- -D warnings
    - cargo build --target armv7-unknown-linux-gnueabihf --all-features --verbose
  after_script: *cleanup

# Cross-compile for armv7-unknown-linux-gnueabihf
rust-msrv-armv7:
  stage: build
  image: rust:$MSRV
  rules: *rules_only_on_tag
  variables:
    CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER: arm-linux-gnueabihf-gcc
  tags:
    - memory-optimized
  script:
    - apt-get update -qq && apt-get install gcc-arm-linux-gnueabihf -y -qq
    - rustup target add armv7-unknown-linux-gnueabihf
    - cargo build --target armv7-unknown-linux-gnueabihf --all-features --verbose
  after_script: *cleanup

rust-binary-armv7:
  stage: deploy
  image: rust:latest
  needs:
    - job: rust-latest-armv7
      artifacts: false
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER: arm-linux-gnueabihf-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  script:
    - apt-get update -qq && apt-get install gcc-arm-linux-gnueabihf -y -qq
    - rustup target add armv7-unknown-linux-gnueabihf
    - |
      set -x
      args="--all-features"
      # Only add --locked if a lockfile exists
      if [[ -f "Cargo.lock" ]]; then
        args="${args} --locked"
      fi
      cargo build --target armv7-unknown-linux-gnueabihf --release --verbose $args
    - mv "target/armv7-unknown-linux-gnueabihf/release/$BINARY_NAME" "${BINARY_NAME}-armv7"
  rules:
    - if: $CI_COMMIT_TAG && $BINARY_NAME
  artifacts:
    paths:
      - "${BINARY_NAME}-armv7"
  after_script: *cleanup

# Cross-compile for aarch64-unknown-linux-gnu
rust-latest-aarch64:
  stage: build
  image: rust:latest
  rules: *rules_only_on_tag
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER: aarch64-linux-gnu-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  script:
    - apt-get update -qq && apt-get install gcc-aarch64-linux-gnu -y -qq
    - rustup target add aarch64-unknown-linux-gnu
    - rustup component add clippy
    - cargo clippy --target aarch64-unknown-linux-gnu --all-features -- -D warnings
    - cargo build --target aarch64-unknown-linux-gnu --all-features --verbose
  after_script: *cleanup

rust-msrv-aarch64:
  stage: build
  image: rust:$MSRV
  rules: *rules_only_on_tag
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER: aarch64-linux-gnu-gcc
  script:
    - apt-get update -qq && apt-get install gcc-aarch64-linux-gnu -y -qq
    - rustup target add aarch64-unknown-linux-gnu
    - cargo build --target aarch64-unknown-linux-gnu --all-features --verbose
  after_script: *cleanup

rust-binary-aarch64:
  stage: deploy
  image: rust:latest
  needs:
    - job: rust-latest-aarch64
      artifacts: false
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER: aarch64-linux-gnu-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  script:
    - apt-get update -qq && apt-get install gcc-aarch64-linux-gnu -y -qq
    - rustup target add aarch64-unknown-linux-gnu
    - |
      set -x
      args="--all-features"
      # Only add --locked if a lockfile exists
      if [[ -f "Cargo.lock" ]]; then
        args="${args} --locked"
      fi
      cargo build --target aarch64-unknown-linux-gnu --release --verbose $args
    - mv "target/aarch64-unknown-linux-gnu/release/$BINARY_NAME" "${BINARY_NAME}-aarch64"
  rules:
    - if: $CI_COMMIT_TAG && $BINARY_NAME
  artifacts:
    paths:
      - "${BINARY_NAME}-aarch64"
  after_script: *cleanup

# Cross-compile for riscv64gc-unknown-linux-gnu
rust-latest-riscv64:
  stage: build
  image: rust:latest
  rules: *rules_only_on_tag
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_RISCV64GC_UNKNOWN_LINUX_GNU_LINKER: riscv64-linux-gnu-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  script:
    - apt-get update -qq && apt-get install gcc-riscv64-linux-gnu -y -qq
    - rustup target add riscv64gc-unknown-linux-gnu
    - rustup component add clippy
    - cargo clippy --target riscv64gc-unknown-linux-gnu --all-features -- -D warnings
    - cargo build --target riscv64gc-unknown-linux-gnu --all-features --verbose
  after_script: *cleanup

rust-msrv-riscv64:
  stage: build
  image: rust:$MSRV
  rules: *rules_only_on_tag
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_RISCV64GC_UNKNOWN_LINUX_GNU_LINKER: riscv64-linux-gnu-gcc
  script:
    - apt-get update -qq && apt-get install gcc-riscv64-linux-gnu -y -qq
    - rustup target add riscv64gc-unknown-linux-gnu
    - cargo build --target riscv64gc-unknown-linux-gnu --all-features --verbose
  after_script: *cleanup

rust-binary-riscv64:
  stage: deploy
  image: rust:latest
  needs:
    - job: rust-latest-riscv64
      artifacts: false
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_RISCV64GC_UNKNOWN_LINUX_GNU_LINKER: riscv64-linux-gnu-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  script:
    - apt-get update -qq && apt-get install gcc-riscv64-linux-gnu -y -qq
    - rustup target add riscv64gc-unknown-linux-gnu
    - |
      set -x
      args="--all-features"
      # Only add --locked if a lockfile exists
      if [[ -f "Cargo.lock" ]]; then
        args="${args} --locked"
      fi
      cargo build --target riscv64gc-unknown-linux-gnu --release --verbose $args
    - mv "target/riscv64gc-unknown-linux-gnu/release/$BINARY_NAME" "${BINARY_NAME}-riscv64"
  rules:
    - if: $CI_COMMIT_TAG && $BINARY_NAME
  artifacts:
    paths:
      - "${BINARY_NAME}-riscv64"
  after_script: *cleanup

# Cross-compile for x86_64-pc-windows-gnu
rust-latest-windows64:
  stage: build
  image: rust:latest
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_X86_64_PC_WINDOWS_GNU_LINKER: x86_64-w64-mingw32-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  script:
    - apt-get update -qq && apt-get install gcc-mingw-w64-x86-64 -y -qq
    - rustup target add x86_64-pc-windows-gnu
    - rustup component add clippy
    - cargo clippy --target x86_64-pc-windows-gnu --all-features -- -D warnings
    - cargo build --target x86_64-pc-windows-gnu --all-features --verbose
  rules:
    - if: $RUST_DISABLE_WINDOWS =~ /^(1|yes|true)$/
      when: never
    - when: on_success
  after_script: *cleanup

rust-msrv-windows64:
  stage: build
  image: rust:$MSRV
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_X86_64_PC_WINDOWS_GNU_LINKER: x86_64-w64-mingw32-gcc
  script:
    - apt-get update -qq && apt-get install gcc-mingw-w64-x86-64 -y -qq
    - rustup target add x86_64-pc-windows-gnu
    - cargo build --target x86_64-pc-windows-gnu --all-features --verbose
  rules:
    - if: $RUST_DISABLE_WINDOWS =~ /^(1|yes|true)$/
      when: never
    - when: on_success
  after_script: *cleanup

rust-binary-windows64:
  stage: deploy
  image: rust:latest
  needs:
    - job: rust-latest-windows64
      artifacts: false
      optional: true
  tags:
    - memory-optimized
  variables:
    CARGO_TARGET_X86_64_PC_WINDOWS_GNU_LINKER: x86_64-w64-mingw32-gcc
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
  script:
    - apt-get update -qq && apt-get install gcc-mingw-w64-x86-64 -y -qq
    - rustup target add x86_64-pc-windows-gnu
    - |
      set -x
      args="--all-features"
      # Only add --locked if a lockfile exists
      if [[ -f "Cargo.lock" ]]; then
        args="${args} --locked"
      fi
      cargo build --target x86_64-pc-windows-gnu --release --verbose $args
    - mv "target/x86_64-pc-windows-gnu/release/${BINARY_NAME}.exe" "${BINARY_NAME}-windows64.exe"
  rules:
    - if: $RUST_DISABLE_WINDOWS =~ /^(1|yes|true)$/ || $BINARY_NAME == null
      when: never
    - when: on_success
  artifacts:
    paths:
      - "${BINARY_NAME}-windows64.exe"
  after_script: *cleanup

# Cross-compile for wasm32-unknown-unknown
rust-latest-wasm32:
  stage: build
  image: rust:latest
  tags:
    - memory-optimized
  variables: *variables
  script:
    - |
      set -x
      rustup target add wasm32-unknown-unknown
      rustup component add clippy
      args="--workspace"
      for package in $WASM_EXCLUDE
      do
        args="${args} --exclude ${package}"
      done
      # TODO: --all-features
      cargo clippy --target wasm32-unknown-unknown $args -- -D warnings
      cargo build --target wasm32-unknown-unknown --verbose $args
  rules:
    - if: $RUST_DISABLE_WASM =~ /^(1|yes|true)$/
      when: never
    - when: on_success
  after_script: *cleanup

rust-msrv-wasm32:
  stage: build
  image: rust:$MSRV
  tags:
    - memory-optimized
  script:
    - |
      set -x
      rustup target add wasm32-unknown-unknown
      args="--workspace"
      for package in $WASM_EXCLUDE
      do
        args="${args} --exclude ${package}"
      done
      # TODO: --all-features
      cargo build --target wasm32-unknown-unknown --verbose $args
  rules:
    - if: $RUST_DISABLE_WASM =~ /^(1|yes|true)$/
      when: never
    - when: on_success
  after_script: *cleanup

upload-binary:
  stage: publish
  image: debian:latest
  rules:
    - if: $CI_COMMIT_TAG && $BINARY_NAME
  needs:
    - job: rust-binary-x86_64
      artifacts: true
    - job: rust-binary-armv7
      artifacts: true
    - job: rust-binary-aarch64
      artifacts: true
    - job: rust-binary-riscv64
      artifacts: true
    - job: rust-binary-windows64
      artifacts: true
      optional: true
  before_script:
    - apt-get update -qq && apt-get install curl -y -qq
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${BINARY_NAME}-x86_64" "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-x86_64"
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${BINARY_NAME}-armv7" "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-armv7"
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${BINARY_NAME}-aarch64" "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-aarch64"
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${BINARY_NAME}-riscv64" "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-riscv64"
      test -f "${BINARY_NAME}-windows64.exe" && curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${BINARY_NAME}-windows64.exe" "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-windows64.exe" ||:

release-binary-nowindows:
  stage: publish
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG && $BINARY_NAME && $RUST_DISABLE_WINDOWS =~ /^(1|yes|true)$/
  needs:
    - job: upload-binary
      artifacts: false
  script:
    - echo 'running release_job for $CI_COMMIT_TAG'
  release:
    tag_name: '$CI_COMMIT_TAG'
    description: 'Release of $CI_COMMIT_TAG, see CHANGELOG for more details.'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: "${BINARY_NAME}-x86_64"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-x86_64"
          filepath: "/binaries/${BINARY_NAME}-x86_64"
        - name: "${BINARY_NAME}-armv7"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-armv7"
          filepath: "/binaries/${BINARY_NAME}-armv7"
        - name: "${BINARY_NAME}-aarch64"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-aarch64"
          filepath: "/binaries/${BINARY_NAME}-aarch64"
        - name: "${BINARY_NAME}-riscv64"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-riscv64"
          filepath: "/binaries/${BINARY_NAME}-riscv64"

release-binary-pluswindows:
  stage: publish
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $RUST_DISABLE_WINDOWS =~ /^(1|yes|true)$/ || $CI_COMMIT_TAG == null || $BINARY_NAME == null
      when: never
    - when: on_success
  needs:
    - job: upload-binary
      artifacts: false
  script:
    - echo 'running release_job for $CI_COMMIT_TAG'
  release:
    tag_name: '$CI_COMMIT_TAG'
    description: 'Release of $CI_COMMIT_TAG, see CHANGELOG for more details.'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: "${BINARY_NAME}-x86_64"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-x86_64"
          filepath: "/binaries/${BINARY_NAME}-x86_64"
        - name: "${BINARY_NAME}-armv7"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-armv7"
          filepath: "/binaries/${BINARY_NAME}-armv7"
        - name: "${BINARY_NAME}-aarch64"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-aarch64"
          filepath: "/binaries/${BINARY_NAME}-aarch64"
        - name: "${BINARY_NAME}-riscv64"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-riscv64"
          filepath: "/binaries/${BINARY_NAME}-riscv64"
        - name: "${BINARY_NAME}-windows64.exe"
          url: "${PACKAGE_REGISTRY_URL}/${BINARY_NAME}-windows64.exe"
          filepath: "/binaries/${BINARY_NAME}-windows64.exe"
